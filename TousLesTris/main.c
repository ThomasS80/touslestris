#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define TRUE 1
#define FALSE 0

void tri_a_bulle_c(int *tab,int N)
{
    int j =0;
    int tmp =0;
    int test =1;
    while(test)
    {
        test = FALSE;
        for(j =0; j < N-1; j++)
        {
            if(tab[j] > tab[j+1])
            {
                tmp = tab[j+1];
                tab[j+1] = tab[j];
                tab[j] = tmp;
                test = TRUE;
            }
        }
    }
}
void tri_selection_c(int *tab, int N)
{
    int i, min, j, tmp;
    for(i =0; i < N -1; i++)
    {
        min = i;
        for(j = i+1; j < N ; j++)
            if(tab[j] < tab[min])
                min = j;
        if(min != i)
        {
            tmp = tab[i];
            tab[i] = tab[min];
            tab[min] = tmp;
        }
    }
}
void tri_insertion_c(int *tab,int N)
{
    int i,p,j;
    int x;
    for(i =1; i < N; i++)
    {
        x = tab[i];
        p = i-1;
        while(tab[p] > x && p-- >0) {}
        p++;
        for(j = i-1; j >= p; j--)
        {
            tab[j+1] = tab[j];
        }
        tab[p] = x;
    }
}
int main(void)
{
    const N = 20000;
    int nb_entiers;
    int i;
    int num;
    int nb_alea;
    int tabS1[N],tabS2[N],tabS3[N],tab[N];
    srand (time(NULL));
    printf("Donner le nombre d'entiers que vous voulez trier : ");
    scanf("%d",&nb_entiers);
    printf("\n");
    printf("Tapez 1 pour tri a bulle, 2 pour tri par selection et 3 pour tri par insertion : \n");
    scanf("%d",&num);
    for(i=0; i<nb_entiers; i++)
    {
        nb_alea = rand()%51;
        tab[i]= nb_alea;
    }
    for(i=0; i<nb_entiers; i++)
    {
        tabS1[i] = tab[i];
        tabS2[i] = tab[i];
        tabS3[i] = tab[i];
    
    if(num == 1)
    {
    printf ("\nTri a bulle ");
    tri_a_bulle_c(tabS1,nb_entiers);
    printf ("\n Voici le tableau trie : %d\n",tabS1[i]);
    }

    if(num == 2)
    {
    printf ("\nTri par selection ");
    tri_selection_c(tabS2,nb_entiers);
    printf ("\n Voici le tableau trie : %d\n",tabS2[i]);
    }

    if(num == 3)
    {
    printf ("\nTri par insertion ");
    tri_insertion_c(tabS3,nb_entiers);
    printf("\n Voici le tableau trie : %d\n",tabS3[i]);
    }
    
    }

    return 0;
}

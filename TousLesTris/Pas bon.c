/**********************************************************************************************
* Ce programme remplit de mani�re al�atoire et automatique un tableau de N valeur choisit par *
*   l'utilisateur, le programme fera appel aux 3 proc�dures de tri lui permettant de trier    *
*       ce tableau de N �l�ment compris entre 0 et 50 puis � la fin affichera le temps        *
*                                  d'�x�cution des 3 tris                                     *
**********************************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define TRUE 1
#define FALSE 0

void tri_a_bulle_c(int *tab,int N) // Proc�dure gerant le tri a bulle
{
    int j =0;
    int tmp =0;
    int test =1;
    while(test)
    {
        test = FALSE;
        for(j =0; j < N-1; j++)
        {
            if(tab[j] > tab[j+1])
            {
                tmp = tab[j+1];
                tab[j+1] = tab[j];
                tab[j] = tmp;
                test = TRUE;
            }
        }
    }
}
void tri_selection_c(int *tab, int N) // Proc�dure gerant le tri par selection
{
    int i, min, j, tmp;
    for(i =0; i < N -1; i++)
    {
        min = i;
        for(j = i+1; j < N ; j++)
            if(tab[j] < tab[min])
                min = j;
        if(min != i)
        {
            tmp = tab[i];
            tab[i] = tab[min];
            tab[min] = tmp;
        }
    }
}
void tri_insertion_c(int *tab,int N) // proc�dure gerant le tri par insertion
{
    int i,p,j;
    int x;
    for(i =1; i < N; i++)
    {
        x = tab[i];
        p = i-1;
        while(tab[p] > x && p-- >0) {}
        p++;
        for(j = i-1; j >= p; j--)
        {
            tab[j+1] = tab[j];
        }
        tab[p] = x;
    }
}
int main(void)
{
    float temps; // initialisation du temps, des variables et des constantes
    temps =0;
    clock_t t1,t2,t3,t4,t5,t6;
    const N = 20000;
    int nb_entiers;
    int i;
    int num;
    int nb_alea;
    int tabS1[N],tabS2[N],tabS3[N],tab[N]; // Creation de 3 tableau ayant les memes valeurs que le tableau initiale
    srand (time(NULL));
    printf("Donner le nombre d'entiers que vous voulez trier : "); //Le nombre d'entier que nous souhaitons trier
    scanf("%d",&nb_entiers);
    printf("\n");
    for(i=0; i<nb_entiers; i++) //Remplit le tableau de valeurs comprises entre 0 et 50
    {
        nb_alea = rand()%51;
        tab[i]= nb_alea;
    }
    for(i=0; i<nb_entiers; i++) //Sauvegarde le tableau initiale dans 3 autres tableaux pour chacun des tris
    {
        tabS1[i] = tab[i];
        tabS2[i] = tab[i];
        tabS3[i] = tab[i];
    }
    t1 = clock();
    tri_a_bulle_c(tabS1,nb_entiers); //Appel a la proc�dure de tri pour calculer le temps d'�x�cution du tri
    t2 = clock();
    printf ("\nTri a bulle : ");
    temps = (float)(t2-t1)/CLOCKS_PER_SEC;
    printf("temps = %f\n", temps);

    t3 = clock();
    tri_selection_c(tabS2,nb_entiers); //Appel a la proc�dure de tri pour calculer le temps d'�x�cution du tri
    t4 = clock();
    printf ("\nTri par selection : ");
    temps = (float)(t4-t3)/CLOCKS_PER_SEC;
    printf("temps = %f\n", temps);

    t5 = clock();
    tri_insertion_c(tabS3,nb_entiers); //Appel a la proc�dure de tri pour calculer le temps d'�x�cution du tri
    t6 = clock();
    printf ("\nTri par insertion : ");
    temps = (float)(t6-t5)/CLOCKS_PER_SEC;
    printf("temps = %f\n", temps);

//Realis� par Thomas Salhi 1ere ann�e DUT informatique groupe D
    return 0;
}
